%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1

%define STDOUT 1

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL       ; exit syscall should be performer -> set rax to 60  
    syscall                     ; perform required syscall: exit code should be stored in rdi

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                ; clear rax register

    .loop:
        cmp byte [rdi + rax], 0 ; check if current symbol is null-terminator
        je .end                 ; then go end the function

        inc rax                 ; otherwise increase the length by one
        jmp .loop               ; then go on the next loop iteration
    
    .end:
        ret                     ; return. rax - str length, rdx - str pointer

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax        ; clean rax
    
    push rdi
    call string_length  ; calculating string length: rax - length, rdx - string pointer 
    pop rdi

    mov rsi, rdi                    ; loading string pointer
    mov rdx, rax                    ; string length
    mov rax, WRITE_SYSCALL          ; write syscall number defined
    mov rdi, STDOUT                 ; descriptor set to stdout
    syscall                         ; perform syscall

    ret                             ; returning from the function

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`       ; defing char to be printed

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                    ; store character ASCII code

    mov rax, WRITE_SYSCALL      ; write syscall number defined
    mov rdi, STDOUT             ; descriptor set to stdout
    mov rsi, rsp                ; set stored character address
    mov rdx, 1                  ; printed length
    syscall                     ; perform syscall

    pop rdi                     ; return character ASCII code

    ret                         ; returning from the function

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                          ;   check if num is negative
    jnl .print_the_number               ;   then go to .print_the_number label

    .negative:
        push rdi                        ;
        mov rdi, '-'                    ;   we have to print '-' char in begining because of negative number
        call print_char                 ;
        pop rdi                         ;
        neg rdi                         ;   invert the number sign

    ; then print the number using print_uint
    .print_the_number:

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, 20                             ;   define counter
    sub rsp, 24                             ;   allocate space for nubers

    mov byte [rsp + r10], 0                 ;   set the last number to be 0
    mov rax, rdi                            ;   copy number to RAX
    mov r11, 10                             ;   store the divisor

    .division:
        xor rdx, rdx                        ;   clean RDX
        div r11                             ;   perform division
        dec r10                             ;   decrement counter
        add dl, '0'                         ;   
        mov [rsp + r10], dl                 ;   move to lowest RDX byte 
        test rax, rax                       ;   compare
        jnz .division                       ;   
    
    .end:
        lea rdi, [rsp + r10]                ;   loading address of result string
        call print_string                   ;   pringting string

    add rsp, 24                             ;   restore stack space
    ret                                     ;   end

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax                                ; clear registers

    .loop:
        mov r10b, byte [rdi + rax]              ;   save first string char

        cmp r10b, byte [rsi + rax]              ;   if current chars are not equal   
        jne .not_equal                          ;   then exit with 0 in RAX

        cmp r10b, 0                             ;   stop if end of string occured
        je .end                                 ;

        inc rax                                 ;   moving to the next char
        jmp .loop                               ;   

    .not_equal: 
        xor rax, rax                            ;   put 0 in RAX
        ret
    
    .end:
        mov rax, 1                              ;   put 1 in RAX
        ret                                     ;   end

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax                                    ;   clear registers
    xor rdi, rdi                                    ;

    sub rsp, 8                                      ;   allocate stack space

    mov rsi, rsp                                    ;   set the destination buffer
    mov rdx, 1                                      ;   set the amount of bytes to be read
    syscall                                         ;   perform syscall

    cmp rax, -1                                     ;   check if failed
    je .failed_or_eof                               ;   then go to .failed_or_eof

    test rax, rax                                   ;   perform logical compare
    jz .failed_or_eof                               ;   got zf then go to .failed_or_eof
    
    mov al, [rsp]                                   ;   put the data into RAX   
    jmp .end                                        ;   go to the end
    
    .failed_or_eof:
        xor rax, rax                                ;   if we got failed_or_eof then return 0
    
    .end:
        add rsp, 8                                  ;   restore stack space
        ret                                         ;   return

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax                                        ;   clear registers

    push r12                                            ;
    push r13                                            ;   store caller-saved registers
    push r14                                            ;

    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    
    .loop:
        call read_char

        cmp al, ` `                                     ;   
        jz .skip_space                                  ;
        cmp al, `\t`                                    ;   skipping space chars
        jz .skip_space                                  ;
        cmp al, `\n`                                    ;
        jz .skip_space                                  ;

        cmp rsi, 0                                      ;   if buffer length is 0
        je .finish                                      ;   then just finish function execution

        cmp r14, r13                                    ;   if word does not fit in buffer
        jnl .buffer_overflow                            ;   then buffer overflow detected

        test rax, rax                                   ;   if got zf then finish   
        jz .finish                                      ;
        
        mov [r12 + r14], al                             ;   storing current char in buffer
        inc r14                                         ;   increasing word length

        jmp .loop                                       ;   next iteration

    .skip_space:
        test r14, r14                                   ;   skipping space characters
        jz .loop                                        ;   otherwise finishing the function execution

    .finish:
        mov byte[r12 + r14], 0                          ;   applying null terminator to the end of word
        mov rax, r12                                    ;   buffer ptr
        mov rdx, r14                                    ;   word length

        jmp .end                                        ;   go to the end

    .buffer_overflow:
        xor rax, rax                                    ;   return 0 in rax in case of buffer overflow

    .end:
        pop r14
        pop r13
        pop r12
        ret                                             ;   the end
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                                        ;
    xor rdx, rdx                                        ;   clearing registers
    xor r10, r10                                        ;

    .loop:
        mov r10b, byte [rdi + rdx]                      ;   load char
        sub r10b, '0'                                   ;   make a number from char
        cmp r10b, 9                                     ;   compare with 9
        ja .end                                         ;   if above than 9 -> end function            
        
        imul rax, rax, 10                               ;   shift already parsed value: multiply by 10
        add rax, r10                                    ;   add new number
        
        inc rdx                                         ;   increment number length
	    jmp .loop                                       ;   go to the next iteration
    
    .end:
	    ret                                             ;   function end

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12                                                ;   save register
    mov r12b, [rdi]                                         ;   save first char

    cmp r12b, '0'                                           ;   if first char is less than 0 in ASCII
    jb .check_sign                                          ;   then check sign

    cmp r12b, '9'                                           ;   if first char is greater than 9 in ASCII   
    jbe .parse_num                                          ;   then error, parse number otherwise
    
    jmp .error

    .check_sign:                                            ;
        cmp r12b, '-'                                       ;
        je .valid_sign                                      ;
                                                            ;   Just sign validation: if sign is invalid then error,
        cmp r12b, '+'                                       ;   increase rdi to skip this char
        jne .error                                          ;

    .valid_sign:                    
        inc rdi                                             ;

    .parse_num:
        call parse_uint                                     ;   if the number is positive -> just parse using parse_uint
        
        test rdx, rdx                                       ;   if number parsing failed then just error
        jz .error                                           ;

        cmp r12b, '-'                                       ;   if the number is not negative
        jne .end                                            ;   then just end function execution

        cmp r12b, '+'                                       ;   if the first char was '+'
        je .positive                                        ;   then length also should be increased by 1

        .negative:
            neg rax                                         ;
            inc rdx                                         ;   in case it is negative just invert parsed one + increase number length
            jmp .end                                        ;

        .positive:
            inc rdx                                         ;   in case it was positive with '+' sign also increase number length
            jmp .end                                        ;

    .error:
        pop r12                                             ;
        xor rdx, rdx                                        ;   restore register, put 0 in rdx, end function execution
        ret                                                 ;

    .end:
        pop r12                                             ;   restore register, end function execution
        ret                                                 ;

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi                                        ;   
    push rsi                                        ;   storing caller-saved registers
    push rdx                                        ;
    call string_length
    pop rdx                                         ;
    pop rsi                                         ;   restoring caller-saved registers
    pop rdi                                         ;

    cmp rax, rdx                                    ;   check if string length is larger than buffer can consume
    jae .buffer_no_space                            ;   then exit
        
    xor rax, rax                                    ;   clear registers
    xor r10, r10                                    ;

    .loop:
        mov r10b, byte [rdi + rax]                  ;   copying character from string to buffer   
        mov byte [rsi + rax], r10b                  ;

        inc rax                                     ;   moving to the next character
        
        test r10b, r10b                             ;   check if string ended
        jnz .loop

    .end:
        mov byte [rsi + rax], 0                     ;   add null terminator in the end of the stored string
        ret                                         ;   end

    .buffer_no_space:
        xor rax, rax                                ;   return 0 in rax
        ret                                         ;   end
